package manjian.phillip.rx_kotlin_mvp.di.components

import android.content.Context
import dagger.Component
import manjian.phillip.rx_kotlin_mvp.di.module.ApplicationModule
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {


    fun exposeRetrofit() : Retrofit

    fun exposeContent() : Context
}