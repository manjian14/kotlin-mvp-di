package manjian.phillip.rx_kotlin_mvp.di.components

import dagger.Component
import manjian.phillip.rx_kotlin_mvp.di.module.AdapterModule
import manjian.phillip.rx_kotlin_mvp.di.module.GithubModule
import manjian.phillip.rx_kotlin_mvp.di.scope.PreActivity
import manjian.phillip.rx_kotlin_mvp.modules.home.MainActivity


@PreActivity
@Component(modules = [GithubModule::class, AdapterModule::class],
    dependencies = [ApplicationComponent::class]
)
interface GithubComponent {

    fun inject(mainActivity: MainActivity)
}