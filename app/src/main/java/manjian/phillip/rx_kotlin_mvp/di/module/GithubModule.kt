package manjian.phillip.rx_kotlin_mvp.di.module

import dagger.Module
import dagger.Provides
import manjian.phillip.rx_kotlin_mvp.api.GitHubApiService
import manjian.phillip.rx_kotlin_mvp.di.scope.PreActivity
import manjian.phillip.rx_kotlin_mvp.mvp.view.MainView
import retrofit2.Retrofit


@Module
class GithubModule constructor(private val mainView: MainView) {


    @PreActivity
    @Provides
    fun provideView(): MainView = mainView

    @PreActivity
    @Provides
    fun getGitHubService(retrofit: Retrofit): GitHubApiService = retrofit.create(GitHubApiService::class.java)

}