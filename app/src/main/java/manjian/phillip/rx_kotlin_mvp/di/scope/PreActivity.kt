package manjian.phillip.rx_kotlin_mvp.di.scope

import javax.inject.Scope


@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PreActivity
