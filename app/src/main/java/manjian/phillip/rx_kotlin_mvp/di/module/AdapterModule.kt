package manjian.phillip.rx_kotlin_mvp.di.module


import dagger.Module
import dagger.Provides
import manjian.phillip.rx_kotlin_mvp.di.scope.PreActivity
import manjian.phillip.rx_kotlin_mvp.modules.home.Adapter
import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData

@Module
    class AdapterModule(private val arrayList: ArrayList<GithubData>) {


    @Provides
    @PreActivity
    fun provideAdapter(): Adapter = Adapter(arrayList)
}