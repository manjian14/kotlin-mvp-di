package manjian.phillip.rx_kotlin_mvp.api

import io.reactivex.Maybe
import io.reactivex.Single
import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData
import retrofit2.Call
import retrofit2.http.GET

interface GitHubApiService {

    @GET("repositories")
    fun getGithubData(): Maybe<List<GithubData>>

    @GET("repositories")
    fun getGithubDatas(): Call<List<GithubData>>
}