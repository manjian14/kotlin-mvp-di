package manjian.phillip.rx_kotlin_mvp.application

import android.app.Application
import manjian.phillip.rx_kotlin_mvp.di.components.ApplicationComponent
import manjian.phillip.rx_kotlin_mvp.di.components.DaggerApplicationComponent
import manjian.phillip.rx_kotlin_mvp.di.module.ApplicationModule

class App : Application() {


    lateinit var daggerApplicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
    }

    private fun initApplicationComponent() {
        daggerApplicationComponent = DaggerApplicationComponent.builder().applicationModule(
            ApplicationModule(this,"https://api.github.com/")).build()

    }

    override fun onTerminate() {
        super.onTerminate()
    }
}