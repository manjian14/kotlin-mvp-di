package manjian.phillip.rx_kotlin_mvp.mvp.view

import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData


interface MainView : BaseView {

    fun onGithubLoaded(list: List<GithubData>)
    fun onShowDialog(s: String)
    fun onHideDialog()
    fun showToast(s: String)
}