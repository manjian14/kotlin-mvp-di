package manjian.phillip.rx_kotlin_mvp.mvp.presenter

import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import manjian.phillip.rx_kotlin_mvp.api.GitHubApiService
import manjian.phillip.rx_kotlin_mvp.base.BasePresenter
import manjian.phillip.rx_kotlin_mvp.mapper.GithubMapper
import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData
import manjian.phillip.rx_kotlin_mvp.mvp.view.MainView
import javax.inject.Inject


class GithubPresenter @Inject constructor() : BasePresenter<MainView>(),
    MaybeObserver<List<GithubData>> {


    @Inject
    lateinit var gitHubApiService: GitHubApiService

    @Inject
    lateinit var githubMapper: GithubMapper


    override fun onSubscribe(d: Disposable) {
    }


    override fun onComplete() {
        view.onHideDialog()
    }

    override fun onSuccess(t: List<GithubData>) {
        val list: List<GithubData> = githubMapper.mapGitHub(t)
        view.onGithubLoaded(list)
    }


    override fun onError(e: Throwable) {
        view.onHideDialog()
        view.showToast("error")
    }


    fun getData() {
        view.onShowDialog("Loading data")
        val maybe: Maybe<List<GithubData>> = gitHubApiService.getGithubData()
        subscribe(maybe, this)
    }


//    private fun getGithubServerData(): List<GithubData>? {
//        return gitHubApiService.getGithubData()
//            .subscribeOn(Schedulers.io())
//            .doOnSuccess(Consumer {
//                if (it.isNotEmpty()) {
//                    print("yaya")
//                } else {
//                    print("yoo")
//                }
//                return@Consumer
//            }).doOnError {
//                print(it.stackTrace)
//                return@doOnError
//            }.blockingGet()
//    }


}
