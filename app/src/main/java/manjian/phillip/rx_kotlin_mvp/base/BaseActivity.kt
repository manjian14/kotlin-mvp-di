package manjian.phillip.rx_kotlin_mvp.base

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import manjian.phillip.rx_kotlin_mvp.application.App
import manjian.phillip.rx_kotlin_mvp.di.components.ApplicationComponent

abstract class BaseActivity : AppCompatActivity() {

    private var progressDialog: ProgressDialog? = null

    protected abstract fun getContentView() : Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentView())
        onViewReady(savedInstanceState, intent)
    }


    @CallSuper
    protected open fun onViewReady(savedInstanceState: Bundle?, intent: Intent?) {
        resolveDaggerDependency()
    }

    protected open fun resolveDaggerDependency(){

    }

    protected open fun getApplicationComponent() : ApplicationComponent {
        return (application as App).daggerApplicationComponent
    }

    protected fun showDialog(message: String) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(this)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setCancelable(true)
        }
        progressDialog!!.setMessage(message)
        progressDialog!!.show()
    }

    protected fun hideDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

}