package manjian.phillip.rx_kotlin_mvp.base

import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import manjian.phillip.rx_kotlin_mvp.mvp.view.BaseView
import javax.inject.Inject


open class BasePresenter<V : BaseView> {


    @Inject
    lateinit var view: V

    protected fun <T> subscribe(maybe: Maybe<T>, maybeObserver: MaybeObserver<T>) {
        maybe.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(maybeObserver)


    }


}