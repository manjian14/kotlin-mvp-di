package manjian.phillip.rx_kotlin_mvp.modules.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import manjian.phillip.rx_kotlin_mvp.R
import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData


class Adapter constructor(private val arrayList: ArrayList<GithubData>) :
    RecyclerView.Adapter<Adapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.repo_list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (arrayList.isNotEmpty()) {
            holder.bind(arrayList[position])
        }
    }


    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class MyViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private val textView: TextView = itemView.findViewById(R.id.repoTitle)
        private val textView1: TextView = itemView.findViewById(R.id.reposubTitle)


        fun bind(s: GithubData) {
            textView.text = s.name
        }

    }
}
