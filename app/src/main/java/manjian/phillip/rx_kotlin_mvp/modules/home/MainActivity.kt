package manjian.phillip.rx_kotlin_mvp.modules.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import manjian.phillip.rx_kotlin_mvp.R
import manjian.phillip.rx_kotlin_mvp.base.BaseActivity
import manjian.phillip.rx_kotlin_mvp.di.components.DaggerGithubComponent
import manjian.phillip.rx_kotlin_mvp.di.components.GithubComponent
import manjian.phillip.rx_kotlin_mvp.di.module.AdapterModule
import manjian.phillip.rx_kotlin_mvp.di.module.GithubModule
import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData
import manjian.phillip.rx_kotlin_mvp.mvp.presenter.GithubPresenter
import manjian.phillip.rx_kotlin_mvp.mvp.view.MainView
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {


    @Inject
    lateinit var githubPresenter: GithubPresenter

    @Inject
    lateinit var adapter: Adapter


    private var arrayList: ArrayList<GithubData> = ArrayList()

    override fun getContentView(): Int {
        return R.layout.activity_main
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent?) {
        super.onViewReady(savedInstanceState, intent)
        initializes()
        githubPresenter.getData()
    }

    private fun initializes() {
        repoList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = this@MainActivity.adapter
        }
    }

    override fun resolveDaggerDependency() {
        super.resolveDaggerDependency()
        val githubComponent: GithubComponent = DaggerGithubComponent.builder()
            .applicationComponent(getApplicationComponent())
            .adapterModule(AdapterModule(arrayList))
            .githubModule(GithubModule(this)).build()
        githubComponent.inject(this)
    }


    override fun onGithubLoaded(list: List<GithubData>) {
        arrayList.addAll(list)
        adapter.notifyDataSetChanged()
    }

    override fun onShowDialog(s: String) {
        showDialog(s)
    }

    override fun onHideDialog() {
        hideDialog()
    }

    override fun showToast(s: String) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show()

    }


}
