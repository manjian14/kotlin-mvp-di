//package manjian.phillip.rx_kotlin_mvp.modules.home;
//
//import android.text.TextUtils;
//import android.util.Log;
//
//import androidx.annotation.NonNull;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.List;
//import java.util.concurrent.Callable;
//
//import io.reactivex.Observable;
//import io.reactivex.ObservableEmitter;
//import io.reactivex.ObservableOnSubscribe;
//import io.reactivex.ObservableSource;
//import io.reactivex.Single;
//import io.reactivex.functions.Consumer;
//import io.reactivex.functions.Function;
//import io.reactivex.observers.DisposableObserver;
//import manjian.phillip.rx_kotlin_mvp.mvp.model.GithubData;
//
//import static androidx.constraintlayout.widget.Constraints.TAG;
//
//public class TT {
//
//    public Observable<String> generateWords() {
//        return Observable.create(new ObservableOnSubscribe<String>() {
//            @Override
//            public void subscribe(@NonNull ObservableEmitter<String> emitter) throws Exception {
//                emitter.onNext("I");
//                emitter.onNext("Am");
//                emitter.onNext("Home");
//                emitter.onComplete();
//            }
//        });
//    }
//
//    public Single<String> mergeWords(final Collection<String> words) {
//        return Single.fromCallable(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return TextUtils.join(" ", words);
//            }
//        });
//    }
//
//
//}
